const corsAnywhere = "https://cors-anywhere.herokuapp.com/";

export default function loadIframe(){
  console.log("Loading iframe...");
  return new Promise(function(resolve, reject) {
    let iframe = document.createElement('iframe');
    iframe.onload = () => {
      console.log("iframe loaded.");
      resolve(iframe);
    };
    let url_string = window.location.href;
    let url = new URL(url_string);
    let params = {};
    for(var pair of url.searchParams.entries()) {
       params[pair[0]] = pair[1];
    }
    fetch(corsAnywhere + "https://web.archive.org/web/" + params["t"] + "im_/http://www.icontact-archive.com/" + params["id"])
        .then((resp) => resp.text())
        .then((content) => {
            iframe.srcdoc = content;
            document.querySelector('body').appendChild(iframe);
          })
    //document.querySelector('body').appendChild(iframe); // add it to wherever you need it in the document
  });
}
