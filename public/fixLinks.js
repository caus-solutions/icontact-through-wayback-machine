export default function fixLinks(iframe){
  console.log("Fixing Links...");
  return new Promise(function(resolve, reject) {
      let iframeDocument = iframe.contentDocument;
      let iframeBody = iframeDocument.querySelector("body");
      iframeBody.querySelectorAll('a').forEach(node => {
          let href = node.href;
          href = matchTwice(stripArchive, href);
          href = matchTwice(stripICPtrack, href);
          node.href = href;
      });
      console.log("Links fixed.");
      resolve();
  });
}

const stripArchive = /https?:\/\/web.archive.org\/web\/[0-9]+\/(.*)/g;
const stripICPtrack =  /https?:\/\/click.icptrack.com\/icp\/rclick.php\?.*destination=(https?.*)/g;

function decode(s) {
    let t = decodeURIComponent(s);
    if(s !== t){
        return t;
    } else {
        return s;
    }
}

function matchTwice(regex, s){
  let match = regex.exec(s);
  if(match){
      s = match[1];
  }
  s = decode(s);
  match = regex.exec(s);
  if(match){
      s = match[1];
  }
  return decode(s);
}
