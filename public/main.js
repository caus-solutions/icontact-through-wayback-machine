import loadIframe from './loadIframe.js';
import fixLinks from './fixLinks.js';

loadIframe().then((iframe)=>{
  fixLinks(iframe).then(()=>{
    console.info("Done with Re-Write.")
  });
});
