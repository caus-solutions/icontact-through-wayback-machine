# iContact through Wayback Machine

This project is to add a viewer for iContact archived messages that are saved on the internet archive's wayback machine.

It re-writes links to go to live sites and to skip iContact monitor links.